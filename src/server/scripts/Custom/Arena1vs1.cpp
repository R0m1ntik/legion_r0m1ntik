/*
 * Script By r0m1ntik
 * Date 05.06.2018
 * 100% Worked
 */

#include "ArenaTeam.h"
#include "ArenaTeamMgr.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "BattlegroundPackets.h"
#include "Chat.h"
#include "DatabaseEnv.h"
#include "DB2Stores.h"
#include "DisableMgr.h"
#include "GossipDef.h"
#include "Language.h"
#include "Log.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"

class npc_arena1vs1 : public CreatureScript
{
public:
    npc_arena1vs1() : CreatureScript("npc_arena1vs1") { }

		bool CreateArenateam(Player* player)
		{
			if(!player)
				return false;

			uint8 slot = ArenaTeam::GetSlotByType(ARENA_TEAM_5v5);
			if (slot >= MAX_ARENA_SLOT)
				return false;

			// Check if player is already in an arena team
			if (player->GetArenaTeamId(slot))
				return false;

			// Teamname = playername
			// if teamname exist, we have to choose another name (playername + number)
			int i = 1;
			std::stringstream teamName;
			teamName << player->GetName();
			do
			{
				if(sArenaTeamMgr->GetArenaTeamByName(teamName.str()) != NULL) // teamname exist, so choose another name
				{
					teamName.str(std::string());
					teamName << player->GetName() << (i++);
				}
				else
					break;
			} 
            while (i < 100); // should never happen

			// Create arena team
			ArenaTeam* arenaTeam = new ArenaTeam();

			if (!arenaTeam->Create(player->GetGUID(), ARENA_TEAM_5v5, teamName.str(), 4283124816, 45, 4294242303, 5, 4294705149))
			{
				delete arenaTeam;
				return false;
			}

			// Register arena team
			sArenaTeamMgr->AddArenaTeam(arenaTeam);
			arenaTeam->AddMember(player->GetGUID());

			ChatHandler(player->GetSession()).SendSysMessage("Команда арены успешно создана");
			return true;
		}

        bool JoinQueueArena(Player* player, bool isRated, uint8 arenatype)
		{
			if(!player)
				return false;

			if (sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) > player->getLevel())
				return false;

			ObjectGuid guid = player->GetGUID();
			uint8 arenaslot = ArenaTeam::GetSlotByType(ARENA_TEAM_5v5);
			uint32 arenaRating = 0;
			uint32 matchmakerRating = 0;

			// ignore if we already in BG or BG queue
			if (player->InBattleground() || player->InBattlegroundQueue())
			return false;

			//check existance
			Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
			if (!bg)
			{
				TC_LOG_ERROR("scripts", "Battleground: template bg (all arenas) not found");
				return false;
			}

			if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
			{
				return false;
			}

			BattlegroundTypeId bgTypeId = bg->GetTypeID();
			BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
			PVPDifficultyEntry const* bracketEntry = DB2Manager::GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());
			if (!bracketEntry)
				return false;

			GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_JOIN_FAILED;

			// check if already in queue
			if (player->GetBattlegroundQueueIndex(bgQueueTypeId) < PLAYER_MAX_BATTLEGROUND_QUEUES)
				return false;
			// check if has free queue slots
			if (!player->HasFreeBattlegroundQueueId())
			    return false;

			uint32 ateamId = 0;

			if(isRated)
			{
				ateamId = player->GetArenaTeamId(arenaslot);
				ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(ateamId);
				if (!at)
				{
					player->GetSession()->SendNotInArenaTeamPacket(arenatype);
					return false;
				}

				// get the team rating for queueing
				arenaRating = at->GetRating();
				matchmakerRating = arenaRating;
				// the arenateam id must match for everyone in the group

				if (arenaRating <= 0)
					arenaRating = 1;
			}

			BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
			bg->SetRated(isRated);

			GroupQueueInfo* ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, isRated, false, arenaRating, matchmakerRating, ateamId);
			uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
			uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);

            WorldPackets::Battleground::BattlefieldStatusQueued battlefieldStatus;
            sBattlegroundMgr->BuildBattlegroundStatusQueued(&battlefieldStatus, bg, player, queueSlot, ginfo->JoinTime, avgTime, arenatype, true);
            player->SendDirectMessage(battlefieldStatus.Write());  

			sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());
        	return true;          
		}        

		bool OnGossipHello(Player* player, Creature* creature) override
        {
		    ClearGossipMenuFor(player);
		    if(sWorld->getBoolConfig(CONFIG_ARENA_1V1_ENABLE) == false)
		    {
			    ChatHandler(player->GetSession()).SendSysMessage("Арена отключена!");
			    return true;
		    }

            std::string name = player->GetName();
            std::ostringstream femb;

		    if(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5)) == 0)
			{
				femb << "Уважаемый|cffB404AE " << name << "|r\n";
	            femb << "Создайте команду арены и здесь будет отображаться ваша статистика.";
				AddGossipItemFor(player,GOSSIP_ICON_BATTLE, femb.str().c_str(), GOSSIP_SENDER_MAIN, 5);
				AddGossipItemFor(player, GOSSIP_ICON_BATTLE, "|TInterface\\icons\\Achievement_FeatsOfStrength_Gladiator_08:20:20:-1|t > Создать команду арены", GOSSIP_SENDER_MAIN, 1, "Вы уверены что хотите создать команду для арены 1x1 ?", sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS), false); 
			}
		    else
		    {
				ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5)));
				if(at)
				{
            		uint32 loos = at->GetStats().SeasonGames - at->GetStats().SeasonWins;
            		femb << "Уважаемый|cffB404AE " << name << "|r\n\n";
					femb << "  * Рейтинг: " << at->GetStats().Rating;
					femb << "\n  * Ранг: " << at->GetStats().Rank;
					femb << "\n  * Игр за сезон: " << at->GetStats().SeasonGames;
					femb << "\n  * Побед за сезон: " << at->GetStats().SeasonWins;
            		femb << "\n  * Поражений за сезон: " << loos;

					AddGossipItemFor(player,GOSSIP_ICON_BATTLE, femb.str().c_str(), GOSSIP_SENDER_MAIN, 5);
				}   
                if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5) == false)
			    {
				    AddGossipItemFor(player, GOSSIP_ICON_BATTLE, "|TInterface\\icons\\Achievement_arena_2v2_7:20:20:-1|t > Регистрировать 1х1", GOSSIP_SENDER_MAIN, 2);
                }
		    }
            AddGossipItemFor(player,GOSSIP_ICON_BATTLE, "|TInterface/PaperDollInfoFrame/UI-GearManager-Undo:20:20:-1|t > Закрыть", GOSSIP_SENDER_MAIN, 5);
			SendGossipMenuFor(player, 1, creature->GetGUID());           
            return true;
        }
		
		bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction) override
	    {	
		    ClearGossipMenuFor(player);
			switch (uiAction)
			{
                case 1: /* Create Team 1vs1 */
                {
					if(sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) <= player->getLevel())
					{
						if(player->GetMoney() >= sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) && CreateArenateam(player))
							player->ModifyMoney(sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) * -1);
					}
					else
					{
						ChatHandler(player->GetSession()).PSendSysMessage("Для того чтобы создать команду на арену вам нужно быть %u ым уровнем", sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL));
                		CloseGossipMenuFor(player);
						return true;
					}                     
                }
                break;

                case 2: /* Join Queue 1vs1 */
                {
				    if(JoinQueueArena(player, true, ARENA_TYPE_5v5) == false)
					    ChatHandler(player->GetSession()).SendSysMessage("Произошла ошибка при вступлении в очередь.");
					CloseGossipMenuFor(player);
					return true;
                }
                break;

                case 5:
                    CloseGossipMenuFor(player);
                break;    
            }
        return true;    
        }	    
};

void AddSC_npc_arena1vs1()
{
    new npc_arena1vs1();
}